#!/usr/bin/env ruby

require "socket"
require "ipaddr"
require "highline"

cli = HighLine.new

MULTICAST_ADDR = "224.0.0.1"
BIND_ADDR = "0.0.0.0"
PORT = 3000

Thread.new do
  socket = UDPSocket.new
  membership = IPAddr.new(MULTICAST_ADDR).hton + IPAddr.new(BIND_ADDR).hton

  socket.setsockopt(:IPPROTO_IP, :IP_ADD_MEMBERSHIP, membership)
  socket.setsockopt(:SOL_SOCKET, :SO_REUSEPORT, 1)

  socket.bind(BIND_ADDR, PORT)

  loop do
      message, _ = socket.recvfrom(255)
      cli.say(message)
  end
end

socket = UDPSocket.open
socket.setsockopt(:IPPROTO_IP, :IP_MULTICAST_TTL, 1)
loop do
  message = cli.ask "Enter your message:"
  socket.send(message, 0, MULTICAST_ADDR, PORT)
end
socket.close
